#include "commondataexchanger.h"

quint64 CommonDataExchanger::additionFilesSize = 0;
quint64 CommonDataExchanger::dirsCommonCount = 0;
quint64 CommonDataExchanger::dirsComputedCount = 0;
QHash<QString, quint64> CommonDataExchanger::fileSizesMap = {};

QMutex* CommonDataExchanger::mMutexAdditionFileSize = 0;
QMutex* CommonDataExchanger::mMutexDirsCommonCount = 0;
QMutex* CommonDataExchanger::mMutexDirsCompCount = 0;
QMutex* CommonDataExchanger::mMutexFileSizesMap = 0;

void CommonDataExchanger::initMutexes()
{
    mMutexAdditionFileSize = new QMutex();
    mMutexDirsCommonCount = new QMutex();
    mMutexDirsCompCount = new QMutex();
    mMutexFileSizesMap = new QMutex();
}

void CommonDataExchanger::deleteMutexes()
{
    if(mMutexAdditionFileSize)
        delete mMutexAdditionFileSize;
    if(mMutexDirsCommonCount)
        delete mMutexDirsCommonCount;
    if(mMutexDirsCompCount)
        delete mMutexDirsCompCount;
    if(mMutexFileSizesMap)
        delete mMutexFileSizesMap;
}

void CommonDataExchanger::refreshDatas()
{
    additionFilesSize = 0;
    dirsCommonCount = 0;
    dirsComputedCount = 0;
    fileSizesMap.clear();
}

quint64 CommonDataExchanger::getAdditionFilesSize()
{
    quint64 addition = 0;
    mMutexAdditionFileSize->lock();

    addition = additionFilesSize;
    additionFilesSize = 0;

    mMutexAdditionFileSize->unlock();
    return addition;
}

void CommonDataExchanger::addAdditionFilesSize(const quint64 &value)
{
    mMutexAdditionFileSize->lock();
    additionFilesSize += value;
    mMutexAdditionFileSize->unlock();
}

quint64 CommonDataExchanger::getDirsCommonCount()
{
    QMutexLocker mutexLocker(mMutexDirsCommonCount);
    return dirsCommonCount;
}

void CommonDataExchanger::addDirsCommonCount(const quint64 &value)
{
    mMutexDirsCommonCount->lock();
    dirsCommonCount += value;
    mMutexDirsCommonCount->unlock();
}

quint64 CommonDataExchanger::getDirsComputedCount()
{
    QMutexLocker mutexLocker(mMutexDirsCompCount);
    return dirsComputedCount;
}

void CommonDataExchanger::addDirsComputedCount(const quint64 &value)
{
    mMutexDirsCompCount->lock();
    dirsComputedCount += value;
    mMutexDirsCompCount->unlock();
}

QHash<QString, quint64> CommonDataExchanger::getFileSizesMap()
{
    QMutexLocker mutexLocker(mMutexFileSizesMap);
    return fileSizesMap;
}

void CommonDataExchanger::addFileSizeToMap(QString name, quint64 size)
{
    QMutexLocker mutexLocker(mMutexFileSizesMap);
    if(fileSizesMap.contains(name))
        size += fileSizesMap.value(name);
    fileSizesMap.insert(name, size);
}
