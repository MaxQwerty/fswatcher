#ifndef STATISTICSCOLLECTOR_H
#define STATISTICSCOLLECTOR_H

#include <QThread>
#include <QDir>
#include "commondataexchanger.h"

class StatisticsCollector : public QThread
{
    QString pathTargetDir;
public:
    StatisticsCollector(QString path);
    void run();
    void collectStatisticsFromDir(QDir);
};

#endif // STATISTICSCOLLECTOR_H
