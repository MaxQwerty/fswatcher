#ifndef COMMONDATAEXCHANGET_H
#define COMMONDATAEXCHANGET_H

#include <QMutex>
#include <QHash>

class CommonDataExchanger
{
    static QMutex* mMutexDirsCommonCount;
    static quint64 dirsCommonCount;

    static QMutex* mMutexDirsCompCount;
    static quint64 dirsComputedCount;

    static QMutex* mMutexAdditionFileSize;
    static quint64 additionFilesSize;

    static QMutex* mMutexFileSizesMap;
    static QHash<QString, quint64> fileSizesMap;

public:
    static void initMutexes();
    static void deleteMutexes();
    static void refreshDatas();
    static quint64 getAdditionFilesSize();
    static void addAdditionFilesSize(const quint64 &value);
    static quint64 getDirsCommonCount();
    static void addDirsCommonCount(const quint64 &value);
    static quint64 getDirsComputedCount();
    static void addDirsComputedCount(const quint64 &value);
    static QHash<QString, quint64> getFileSizesMap();
    static void addFileSizeToMap(QString name, quint64 size);
};

#endif // COMMONDATAEXCHANGET_H
