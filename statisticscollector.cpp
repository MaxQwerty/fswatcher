#include "statisticscollector.h"
#include <QMimeDatabase>

#include <QDebug>

StatisticsCollector::StatisticsCollector(QString path) :
    pathTargetDir(path)
{
}

void StatisticsCollector::run()
{
    collectStatisticsFromDir(pathTargetDir);
}

void StatisticsCollector::collectStatisticsFromDir(QDir dir)
{
    QStringList dirList = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    CommonDataExchanger::addDirsCommonCount(dirList.size());
    QStringList filesList = dir.entryList(QDir::Files, QDir::Type);
    quint64 filesSize = 0;
    quint64 curFileSize = 0;
    QMimeDatabase db;
    foreach(QString str, filesList) {
        curFileSize = QFileInfo(dir.path() + "/" + str).size();
        filesSize += curFileSize;
        CommonDataExchanger::addFileSizeToMap(db.mimeTypeForFile(dir.path() + "/" + str).name(), curFileSize);
    }
    CommonDataExchanger::addAdditionFilesSize(filesSize);
    foreach(QString str, dirList) {
        collectStatisticsFromDir(QDir(dir.path() + "/" + str));
        CommonDataExchanger::addDirsComputedCount(1);
    }
}
