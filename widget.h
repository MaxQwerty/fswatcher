#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QSplitter>
#include <QTreeView>
#include <QDirModel>
#include "statisticviewer.h"

class Widget : public QWidget
{
    Q_OBJECT

    QSplitter* mSplitter;
    QTreeView* mTreeView;
    QDirModel* mDirModel;

    StatisticViewer* mStatisticContainer;

public:
    Widget(QWidget *parent = 0);
    ~Widget();
};

#endif // WIDGET_H
