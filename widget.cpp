#include "widget.h"
#include <QLayout>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    mSplitter = new QSplitter(Qt::Horizontal);
    mTreeView = new QTreeView();
    mStatisticContainer = new StatisticViewer();
    mDirModel = new QDirModel();

    mTreeView->setModel(mDirModel);
    mTreeView->setMinimumWidth(400);

    mSplitter->addWidget(mTreeView);
    mSplitter->addWidget(mStatisticContainer);

    QVBoxLayout* lay = new QVBoxLayout();
    lay->addWidget(mSplitter);
    setLayout(lay);

    connect(mTreeView, &QTreeView::activated,
            [this](QModelIndex idx){mStatisticContainer->setTargetDirectory(mDirModel->filePath(idx));});
}

Widget::~Widget()
{

}
