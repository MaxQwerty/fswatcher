#-------------------------------------------------
#
# Project created by QtCreator 2015-08-31T12:12:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FSWatcher
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        widget.cpp \
    commondataexchanger.cpp \
    statisticscollector.cpp \
    statisticviewer.cpp

HEADERS  += widget.h \
    commondataexchanger.h \
    statisticscollector.h \
    statisticviewer.h
