#include "statisticviewer.h"
#include <QDir>
#include <QTableWidgetItem>

#include <QDebug>

StatisticViewer::StatisticViewer(QWidget *parent) : QWidget(parent)
{
    setMinimumWidth(300);
    curFilesSize = 0;
    QVBoxLayout* lay = new QVBoxLayout();

    mProgress = new QProgressBar();
    mProgress->setOrientation(Qt::Horizontal);
    mProgress->setRange(0, 100);
    mProgress->setFormat("%v/%m");
    mProgress->setValue(0);
    lay->addWidget(mProgress);

    lay->addWidget(new QLabel("<b>Directory path:</b>"));
    mLabelDirName = new QLabel("/");
    lay->addWidget(mLabelDirName);

    lay->addWidget(new QLabel("<b>Entry directories:</b>"));
    mListEntryDirnames = new QListWidget();
    lay->addWidget(mListEntryDirnames);

    lay->addWidget(new QLabel("<b>Common files size:</b>"));
    mLabelFilesSize = new QLabel("0");
    lay->addWidget(mLabelFilesSize);

    lay->addWidget(new QLabel("<b>File types statistic:</b>"));
    mTableFilesSizes = new QTableWidget();
    mTableFilesSizes->setColumnCount(2);
    lay->addWidget(mTableFilesSizes);

    CommonDataExchanger::initMutexes();
    CommonDataExchanger::refreshDatas();

    collectorThread = 0;

    setLayout(lay);

    statisticUpdateTimer = new QTimer();
    statisticUpdateTimer->setInterval(250);
    statisticUpdateTimer->setSingleShot(false);
    connect(statisticUpdateTimer, &QTimer::timeout, [this](){this->updateStatisticData();});
    connect(mProgress, &QProgressBar::valueChanged,
            [this]()
            {
                if(mProgress->value() == mProgress->maximum())
                    statisticUpdateTimer->stop();
            });
}

StatisticViewer::~StatisticViewer()
{
    CommonDataExchanger::deleteMutexes();
}

void StatisticViewer::setTargetDirectory(const QString path)
{
    curFilesSize = 0;
    if(collectorThread)
    {
        collectorThread->terminate();
        collectorThread->wait();
        delete collectorThread;
        CommonDataExchanger::refreshDatas();
    }
    QDir curDir(path);
    collectorThread = new StatisticsCollector(path);
    mListEntryDirnames->clear();
    QStringList dirList = curDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    foreach(QString str, dirList) {
        new QListWidgetItem(str, mListEntryDirnames);
    }
    mLabelDirName->setText(QString("%0").arg(path));
    collectorThread->start();
    statisticUpdateTimer->start();
}

void StatisticViewer::updateStatisticData()
{
    curFilesSize += CommonDataExchanger::getAdditionFilesSize();
    mLabelFilesSize->setText(QString::number(curFilesSize));

    mProgress->setRange(0, CommonDataExchanger::getDirsCommonCount());
    mProgress->setValue(CommonDataExchanger::getDirsComputedCount());

    QHash<QString, quint64> filesSizes  = CommonDataExchanger::getFileSizesMap();
    QList<QString> keysList = filesSizes.keys();
    QTableWidgetItem* ptwi;
    mTableFilesSizes->clear();
    mTableFilesSizes->setRowCount(keysList.size());
    for (int i = 0; i < keysList.size(); ++i) {
        ptwi = new QTableWidgetItem(keysList.at(i));
        mTableFilesSizes->setItem(i, 0, ptwi);
        ptwi = new QTableWidgetItem(QString::number(filesSizes.value(keysList.at(i))));
        mTableFilesSizes->setItem(i, 1, ptwi);
    }
}

