#ifndef STATISTICCONTAINER_H
#define STATISTICCONTAINER_H

#include <QWidget>
#include <QLayout>
#include <QProgressBar>
#include <QLabel>
#include <QListWidget>
#include <QTableWidget>
#include <QTimer>
#include <commondataexchanger.h>
#include <statisticscollector.h>

class StatisticViewer : public QWidget
{
    Q_OBJECT
    QProgressBar* mProgress;
    QLabel* mLabelDirName;
    QListWidget* mListEntryDirnames;
    QLabel* mLabelFilesSize;
    QTableWidget* mTableFilesSizes;
    StatisticsCollector* collectorThread;
    QTimer* statisticUpdateTimer;

    qint64 curFilesSize;

public:
    StatisticViewer(QWidget *parent = 0);
    ~StatisticViewer();

signals:

public slots:
    void setTargetDirectory(const QString path);
    void updateStatisticData();
};

#endif // STATISTICCONTAINER_H
